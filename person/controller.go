package person

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
)

type PersonController struct {
	Handler *PersonHandler
}

func (c *PersonController) GetPersonController(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	if nik == "" {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	person, err := c.Handler.GetPerson(nik)
	if err != nil {
		http.Error(w, fmt.Sprintf("person not found"), 404)
		return
	}

	json.NewEncoder(w).Encode(person)
}

func (c *PersonController) AddPersonController(w http.ResponseWriter, r *http.Request) {
	var person Person
	json.NewDecoder(r.Body).Decode(&person)
	if &person.NIK == nil || person.NIK == "" {
		http.Error(w, fmt.Sprintf("nik must required"), 422)
		return
	}
	val, err := c.Handler.AddPerson(person)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte(val))
	w.WriteHeader(201)
}

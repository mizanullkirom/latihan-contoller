package person

import (
	"errors"

	"go.mongodb.org/mongo-driver/bson"
)

type PersonHandler struct{}

func (ph *PersonHandler) GetPerson(nik string) (*Person, error) {
	if nik == "" {
		return nil, errors.New("nik must required")
	}
	person := &Person{}
	err := GetOne(person, bson.M{"nik": nik})
	if err != nil {
		return nil, errors.New("person not found")
	}
	return person, nil
}

func (ph *PersonHandler) AddPerson(person Person) (string, error) {
	existingPerson := &Person{}
	err := GetOne(existingPerson, bson.M{"nik": person.NIK})
	if err == nil {
		return "", errors.New("person already exist")
	}
	_, err = AddOne(&person)
	if err != nil {
		return "", err
	}
	return "Person created successfully", nil
}

package person_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan-contoller/common"
	"gitlab.com/mizanullkirom/latihan-contoller/person"
)

func TestAddPerson(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()
	personData := person.Person{
		Name:        "Test",
		NIK:         "00010010",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
	}
	ph := &person.PersonHandler{}

	val, err := ph.AddPerson(personData)
	asserts.NoError(err)
	asserts.Equal(val, "Person created successfully")
}

func TestAddPersonNikDuplicate(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()
	personData := person.Person{
		Name:        "Test",
		NIK:         "00010010",
		PhoneNumber: "085742666592",
		Address:     "Tegal",
	}
	ph := &person.PersonHandler{}

	_, err := ph.AddPerson(personData)
	asserts.Error(err, errors.New("person already exist"))

}

func TestGetPersonNotFound(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()
	ph := &person.PersonHandler{}
	_, err := ph.GetPerson("000")
	asserts.Error(err, errors.New("person not found"))
}

func TestGetPersonWithoutNIK(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()
	ph := &person.PersonHandler{}
	_, err := ph.GetPerson("")
	asserts.Error(err, errors.New("nik must required"))
}

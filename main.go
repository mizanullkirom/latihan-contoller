package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mizanullkirom/latihan-contoller/common"
	"gitlab.com/mizanullkirom/latihan-contoller/person"
)

func registerRoutes() http.Handler {
	r := chi.NewRouter()
	ph := &person.PersonHandler{}
	pc := &person.PersonController{
		Handler: ph,
	}
	r.Route("/person", func(r chi.Router) {
		r.Get("/{id}", pc.GetPersonController)
		r.Post("/", pc.AddPersonController)
		// r.Put("/{id}", ihandler.UpdateItem)
		// r.Delete("/{id}", ihandler.DeleteItem)
	})
	return r
}

func main() {
	common.MongoInit()
	var r = registerRoutes()
	http.ListenAndServe(":3000", r)
}
